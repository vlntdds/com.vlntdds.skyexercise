package com.vlntdds.skymobileexercise.network;

import com.vlntdds.skymobileexercise.model.Movie;

import java.util.List;
import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by eduardocjr on 10/08/17.
 */

public interface NetworkService {

    @GET("Movies")
    Observable<List<Movie>> getResults();

}
