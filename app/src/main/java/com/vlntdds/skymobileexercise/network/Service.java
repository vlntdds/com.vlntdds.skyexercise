package com.vlntdds.skymobileexercise.network;


import com.vlntdds.skymobileexercise.model.Movie;

import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;


/**
 * Created by eduardocjr on 10/08/17.
 */

public class Service {

    private final NetworkService netService;

    public Service(NetworkService netService) {
        this.netService = netService;
    }

    public rx.Observable getMovies() {
        return netService.getResults()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, rx.Observable<? extends List<Movie>>>() {
                    @Override
                    public rx.Observable<? extends List<Movie>> call(Throwable throwable) {
                        return rx.Observable.error(throwable);
                    }
                });
    }

    public interface GetResultsCallback {
        void getMoviesCatalogOK(List<Movie> moviesCatalog);
        void getMoviesCatalogERR();
    }
}
