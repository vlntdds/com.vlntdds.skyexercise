package com.vlntdds.skymobileexercise;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.vlntdds.skymobileexercise.components.Components;
import com.vlntdds.skymobileexercise.components.DaggerComponents;
import com.vlntdds.skymobileexercise.network.NetworkModule;

/**
 * Created by eduardocjr on 09/08/17.
 */

public class App extends AppCompatActivity {

    Components components;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        components = DaggerComponents.builder().networkModule(new NetworkModule()).build();
    }

    public Components getComponents() {
        return components;
    }
}
