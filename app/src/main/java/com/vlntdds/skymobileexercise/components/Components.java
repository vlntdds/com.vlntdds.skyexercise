package com.vlntdds.skymobileexercise.components;

import com.vlntdds.skymobileexercise.network.NetworkModule;
import com.vlntdds.skymobileexercise.view.MoviesCatalogActivity;
import javax.inject.Singleton;
import dagger.Component;

/**
 * Created by eduardocjr on 10/08/17.
 */

@Singleton
@Component(modules = NetworkModule.class)
public interface Components {

    void inject(MoviesCatalogActivity activity);

}
