package com.vlntdds.skymobileexercise.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.List;

/**
 * Created by eduardocjr on 09/08/17.
 */

public class Movie implements Serializable {

    @SerializedName("id")
    private String id;

    @SerializedName("title")
    private String title;

    @SerializedName("overview")
    private String overview;

    @SerializedName("duration")
    private String duration;

    @SerializedName("release_year")
    private String releaseYear;

    @SerializedName("cover_url")
    private String coverUrl;

    @SerializedName("backdrops_url")
    private List<String> backdropsUrl;

    public void setDuration(String duration){
        this.duration = duration;
    }

    public String getDuration(){
        return duration;
    }

    public void setOverview(String overview){
        this.overview = overview;
    }

    public String getOverview(){
        return overview;
    }

    public void setCoverUrl(String coverUrl){
        this.coverUrl = coverUrl;
    }

    public String getCoverUrl(){
        return coverUrl;
    }

    public void setReleaseYear(String releaseYear){
        this.releaseYear = releaseYear;
    }

    public String getReleaseYear(){
        return releaseYear;
    }

    public void setId(String id){
        this.id = id;
    }

    public String getId(){
        return id;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getTitle(){
        return title;
    }

    public void setBackdropsUrl(List<String> backdropsUrl){
        this.backdropsUrl = backdropsUrl;
    }

    public List<String> getBackdropsUrl(){
        return backdropsUrl;
    }

}
