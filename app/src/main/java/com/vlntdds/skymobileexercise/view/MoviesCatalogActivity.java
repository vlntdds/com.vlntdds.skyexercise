package com.vlntdds.skymobileexercise.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.GridLayoutManager;
import android.view.View;

import com.vlntdds.skymobileexercise.App;
import com.vlntdds.skymobileexercise.R;
import com.vlntdds.skymobileexercise.model.Movie;
import com.vlntdds.skymobileexercise.network.Service;
import com.vlntdds.skymobileexercise.presenter.MoviesPresenter;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by eduardocjr on 10/08/17.
 */

public class MoviesCatalogActivity extends App implements MoviesCatalogView {

    @Inject
    public Service service;

    RecyclerView recyclerView;
    MoviesPresenter moviesPresenter;
    CoordinatorLayout coordinatorLayout;

    @Override
    public void getMoviesCatalogOK(List<Movie> moviesCatalog) {
        MoviesCatalogAdapter a = new MoviesCatalogAdapter(this, moviesCatalog);
        recyclerView.setAdapter(a);
    }

    @Override
    public void getMoviesCatalogERR() {
        Snackbar.make(coordinatorLayout, R.string.loading_err, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.retry, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        moviesPresenter.getMovies();
                    }
                })
                .show();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getComponents().inject(this);
        coordinatorLayout = findViewById(R.id.coordinator);
        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        moviesPresenter = new MoviesPresenter(service, this);
        moviesPresenter.getMovies();
    }

}
