package com.vlntdds.skymobileexercise.view;

import com.vlntdds.skymobileexercise.model.Movie;
import java.util.List;

/**
 * Created by eduardocjr on 10/08/17.
 */

public interface MoviesCatalogView {
    void getMoviesCatalogOK(List<Movie> moviesCatalog);
    void getMoviesCatalogERR();
}
