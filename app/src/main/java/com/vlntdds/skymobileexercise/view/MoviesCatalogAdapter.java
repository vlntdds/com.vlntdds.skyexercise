package com.vlntdds.skymobileexercise.view;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.vlntdds.skymobileexercise.R;
import com.vlntdds.skymobileexercise.model.Movie;

import java.util.List;

/**
 * Created by eduardocjr on 10/08/17.
 */

class MoviesCatalogAdapter extends RecyclerView.Adapter<MoviesCatalogAdapter.ViewHolder> {

    private Context c;
    private List<Movie> results;

    MoviesCatalogAdapter(Context c, List<Movie> r) {
        this.c = c;
        this.results = r;
    }

    @Override
    public MoviesCatalogAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_layout, null);
        v.setLayoutParams(
                new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT,
                        RecyclerView.LayoutParams.WRAP_CONTENT));
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final MoviesCatalogAdapter.ViewHolder holder, final int position) {
        final Movie movie = results.get(position);
        holder.movieTitle.setText(movie.getTitle());
        final Picasso p = new Picasso.Builder(c).listener(new Picasso.Listener() {
            @Override
            public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception) {
                Log.d(MoviesCatalogAdapter.class.getName(), "Picasso - Error Loading Image: " + movie.getTitle());
                picasso.load(R.drawable.ic_err).into(holder.movieCover);
            }
        }).build();

        p.load(movie.getCoverUrl())
                .networkPolicy(NetworkPolicy.OFFLINE)
                .into(holder.movieCover, new Callback() {
                    @Override
                    public void onSuccess() { }

                    @Override
                    public void onError() {
                        p.load(movie.getCoverUrl()).into(holder.movieCover);
                    }
                });
    }

    @Override
    public int getItemCount() {
        return results.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView movieTitle;
        ImageView movieCover;

        ViewHolder(View itemView) {
            super(itemView);
            movieCover = itemView.findViewById(R.id.cover);
            movieTitle = itemView.findViewById(R.id.title);
        }
    }
}
