package com.vlntdds.skymobileexercise.presenter;

import android.util.Log;

import com.vlntdds.skymobileexercise.model.Movie;
import com.vlntdds.skymobileexercise.network.Service;
import com.vlntdds.skymobileexercise.view.MoviesCatalogActivity;
import com.vlntdds.skymobileexercise.view.MoviesCatalogView;

import java.util.List;
import rx.Observable;
import rx.Subscriber;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by eduardocjr on 10/08/17.
 */

public class MoviesPresenter {

    private CompositeSubscription subs;
    private MoviesCatalogView v;
    private Service s;

    public MoviesPresenter(Service s, MoviesCatalogActivity v) {
        this.s = s;
        this.v = v;
        this.subs = new CompositeSubscription();
    }

    public void getMovies() {
        Observable subscription = s.getMovies();

        subscription.subscribe(new Subscriber<List<Movie>>() {
            @Override
            public void onCompleted() {
                Log.d(MoviesPresenter.class.getName(), "GET: Movies list - Completed");
            }

            @Override
            public void onError(Throwable e) {
                Log.d(MoviesPresenter.class.getName(), "GET: Movies list - Err:" + e.getMessage());
                v.getMoviesCatalogERR();
            }

            @Override
            public void onNext(List<Movie> moviesResponseList) {
                Log.d(MoviesPresenter.class.getName(), "GET: Movies list - Response OK");
                v.getMoviesCatalogOK(moviesResponseList);
            }
        });
    }

    public void stop() {
        subs.unsubscribe();
    }
}
