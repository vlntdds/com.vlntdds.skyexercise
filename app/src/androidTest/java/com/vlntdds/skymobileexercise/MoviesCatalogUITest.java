package com.vlntdds.skymobileexercise;

import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import com.vlntdds.skymobileexercise.network.Service;
import com.vlntdds.skymobileexercise.presenter.MoviesPresenter;
import com.vlntdds.skymobileexercise.view.MoviesCatalogActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;

@RunWith(AndroidJUnit4.class)
public class MoviesCatalogUITest {

    @Mock
    MoviesCatalogActivity v;

    @Mock
    Service c;

    @Rule
    public ActivityTestRule<MoviesCatalogActivity> testRule = new ActivityTestRule<>(MoviesCatalogActivity.class);

    @Before
    public void beforeTest(){
        MockitoAnnotations.initMocks(this);
        new MoviesPresenter(c, v);
    }

    @Test
    public void testBaseActivity() throws Exception {
        onView(ViewMatchers.withText(R.string.movie_catalog_header)).check(matches(isDisplayed()));
        onView(ViewMatchers.withId(R.id.recycler_view)).perform(ViewActions.swipeUp());
        onView(ViewMatchers.withId(R.id.recycler_view)).perform(ViewActions.swipeDown());
    }
}
